package com.projet1.gui;

import com.projet1.File;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FileScreen {
    File file;

    public FileScreen(File file) {
        this.file = file;
    }

    public void showScreen(Stage pStage) {
        javafx.scene.text.Text textA = new javafx.scene.text.Text(this.file.getName());
        textA.setStyle("-fx-font-size: 32;");

        TextArea textContent = new TextArea(this.file.getText());
        textContent.setStyle("-fx-font-size: 12;");
        textContent.setPrefRowCount(30);

        textContent.setOnKeyReleased(e -> {
            String inputText = textContent.getText(); 
            file.setText(inputText);
        });

        VBox hbRoot = new VBox(10);
        hbRoot.setAlignment(Pos.TOP_LEFT);
        hbRoot.getChildren().addAll(textA, textContent);
        pStage.setTitle("FileExplorer");
        pStage.setWidth(700);
        pStage.setHeight(800);
        pStage.setScene(new Scene(hbRoot));
        pStage.show();
    }
}
