package com.projet1.gui;

import com.projet1.Directory;

import javafx.application.Application;
import javafx.stage.Stage;

public class FileExplorerGUI extends Application{
    static Directory root = new Directory("root");

    public static void main(Stage pStage) {
        launch();
    }

    public void start(Stage pStage) throws Exception {
        root.setPath(root.getName());
        DirectoryScreen directoryScreen = new DirectoryScreen(root);
        System.out.println(root.getPath());
        directoryScreen.showScreen(pStage);
    }

}
