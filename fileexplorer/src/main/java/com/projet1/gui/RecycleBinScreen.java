package com.projet1.gui;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.projet1.Directory;
import com.projet1.Entry;
import com.projet1.RecycleBin;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class RecycleBinScreen extends Region{
    RecycleBin recycleBin;
    VBox entryListContainer;

    public RecycleBinScreen(RecycleBin recycleBin){
        this.recycleBin = recycleBin;
    }


    public void showScreen(Stage pStage){
        //Text
        javafx.scene.text.Text textA = new javafx.scene.text.Text("\uD83D\uDDD1️"+"RecycleBin");
        textA.setStyle("-fx-font-size: 32;");
        entryListContainer = new VBox();
        entryListContainer.setSpacing(5);
    
        // Button
        Button tbA = new Button("EmptyBin");
        Button tbC = backRootButton();
    
        //Action of button
        tbA.setOnAction(e -> {
            recycleBin.emptyBin();
            displayDeletedEntryList(recycleBin.getDeletedEntries(),entryListContainer);
        });
        HBox hbRoot = new HBox(10);
        hbRoot.setAlignment(Pos.TOP_LEFT);
        hbRoot.setPadding(new Insets(10));
        Separator verticalSeparator = new Separator();
        verticalSeparator.setOrientation(Orientation.VERTICAL);
        hbRoot.getChildren().addAll(textA,tbA,tbC,verticalSeparator,entryListContainer);
        displayDeletedEntryList(recycleBin.getDeletedEntries(),entryListContainer);
        pStage.setTitle("FileExplorer");
        pStage.setWidth(1200);
        pStage.setHeight(800);
        pStage.setScene(new Scene(hbRoot));
        pStage.show();
        }


        public void displayDeletedEntryList(List<Entry> list, VBox entryBox){
            entryBox.getChildren().clear();
            for(Entry entry:list){
                HBox entryContainer = new HBox(10);
                entryContainer.setAlignment(Pos.CENTER_LEFT);
                if(entry instanceof Directory){
                    Label nameLabel = new Label("\uD83D\uDCC1"+entry.getName());
                    nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
                    nameLabel.setTextFill(Color.BLUE);
                    Button restoreButton = restoreButton(entry,entryListContainer);
                    Label dateInfo = getDatetimeStringLabel(entry.getCreationDateTime());      
                    Button deleteButton = deleteButton(entry);
                    entryContainer.getChildren().addAll(nameLabel,restoreButton,deleteButton,dateInfo);
                }else{
                    Label nameLabel = new Label("\uD83D\uDCC4 "+entry.getName());
                    nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
                    nameLabel.setTextFill(Color.GREEN);
                    Button restoreButton = restoreButton(entry,entryListContainer);
                    Label dateInfo = getDatetimeStringLabel(entry.getCreationDateTime());
                    Button deleteButton = deleteButton(entry);
                    entryContainer.getChildren().addAll(nameLabel,deleteButton,restoreButton,dateInfo);
                } 
                entryBox.getChildren().add(entryContainer); 
            }
        }

        public Button backRootButton(){
            Button backButton = new Button("Back");
            backButton.setOnAction(e -> {
                Stage newStage = new Stage();
                Directory directoryRoot = FileExplorerGUI.root;
                DirectoryScreen newDirectoryScreen = new DirectoryScreen(directoryRoot);
                newDirectoryScreen.showScreen(newStage);
        });
        return backButton;
        }

        public Button restoreButton(Entry entry,VBox box){
            Button restoreButton = new Button("restore");
            restoreButton.setOnAction(e -> {
                recycleBin.restoreItem(entry);
                Directory parent = entry.getParent();
                parent.addEntry(entry);
                displayDeletedEntryList(recycleBin.getDeletedEntries(), box);

        });
        return restoreButton;
        }

        public Label getDatetimeStringLabel(LocalDateTime dateTimeA){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDateTime = dateTimeA.format(formatter);
            Label dateInfo = new Label("EditedAt:"+formattedDateTime);
            return dateInfo;
        }

        public Button deleteButton(Entry entry){
            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(e->{
                recycleBin.restoreItem(entry);
                displayDeletedEntryList(recycleBin.getDeletedEntries(),entryListContainer);
                return;
            });
            return deleteButton;
        }

}
