package com.projet1.gui;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import com.projet1.Directory;
import com.projet1.File;
import com.projet1.RecycleBin;
import com.projet1.Entry;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class DirectoryScreen extends Region{
    Directory directory;
    VBox entryListContainer;

    public DirectoryScreen(Directory directory){
        this.directory = directory;
    }


    public void showScreen(Stage pStage){
    //Text
    javafx.scene.text.Text pathName = new javafx.scene.text.Text(directory.getPath());
    pathName.setStyle("-fx-font-size: 16;");
    
    javafx.scene.text.Text textA = new javafx.scene.text.Text(this.directory.getName());
    textA.setStyle("-fx-font-size: 32;");
    entryListContainer = new VBox();
    entryListContainer.setSpacing(5);

    // Button
    Button tbA = new Button("Add Directory");
    Button tbB = new Button("Add File");
    Button tbC = backButton();
    Button editButton = new Button("Edit");
    Button openBinButton = openBinScreenButton();
    //Action of button
    tbA.setOnAction(e -> {
        Directory newDirectory = new Directory(getInputName(true));
        directory.addEntry(newDirectory);
        displayEntryList(directory.getEntries(),entryListContainer);
    });
    tbB.setOnAction(e -> {
        File newFile = new File(getInputName(false));
        directory.addEntry(newFile);
        displayEntryList(directory.getEntries(),entryListContainer);
    });
    editButton.setOnAction(e->{
        String result=getInputName(true) ;
        directory.editName(result);
        textA.setText(result);
    });

    VBox topText = new VBox();

    HBox directoryInfo =new HBox();
    directoryInfo.getChildren().addAll(textA,tbA,tbB,tbC,editButton,openBinButton);
    topText.getChildren().addAll(pathName,directoryInfo);

    HBox hbRoot = new HBox(10);
    hbRoot.setAlignment(Pos.TOP_LEFT);
    hbRoot.setPadding(new Insets(10));
    Separator verticalSeparator = new Separator();
    verticalSeparator.setOrientation(Orientation.VERTICAL);
    hbRoot.getChildren().addAll(topText,verticalSeparator,entryListContainer);
    displayEntryList(directory.getEntries(),entryListContainer);
    pStage.setTitle("FileExplorer");
    pStage.setWidth(1200);
    pStage.setHeight(800);
    pStage.setScene(new Scene(hbRoot));
    pStage.show();
    }
    
    public String getInputName(boolean isDirectory){
        Dialog<ButtonType> customDialog = new Dialog<>();
        
        if(isDirectory){
            customDialog.setTitle("Create Directory");
        }else{
            customDialog.setTitle("Create File");
        }
        
        TextField nameTextField = new TextField();
        if(isDirectory){
            nameTextField.setPromptText("Enter your Directory name");
        }else{
            nameTextField.setPromptText("Enter your File name");
        }
        customDialog.getDialogPane().setContent(nameTextField);
        customDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        Optional<ButtonType> result = customDialog.showAndWait();

    if (result.isPresent() && result.get() == ButtonType.OK) {
        String enteredText = nameTextField.getText();
        return enteredText;
    } else {
        return null; 
    }
    }    

    public void displayEntryList(List<Entry> list, VBox entryBox){
        entryBox.getChildren().clear();
        for(Entry entry:list){
            HBox entryContainer = new HBox(10);
            entryContainer.setAlignment(Pos.CENTER_LEFT);
            if(entry instanceof Directory){
                Label nameLabel = new Label("\uD83D\uDCC1"+entry.getName());
                nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
                nameLabel.setTextFill(Color.BLUE);
                Directory subDirectory = (Directory) entry; 
                Button customButton = openDirectoryButton(subDirectory, entryBox);
                Button editButton2 = new Button("edit");
                editButton2.setOnAction(e->{
                    String result = getInputName(true);
                    subDirectory.editName(result);
                    //nameLabel.setText("\uD83D\uDCC1"+result);
                    displayEntryList(entry.getParent().getEntries(), entryBox);
                });
                Label dateInfo = getDatetimeStringLabel(entry.getCreationDateTime());
                Button deleteButton = deleteButton(entry,entryListContainer);
                entryContainer.getChildren().addAll(nameLabel,customButton,editButton2, deleteButton,dateInfo);
            }else{
                Label nameLabel = new Label("\uD83D\uDCC4 "+entry.getName());
                nameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
                nameLabel.setTextFill(Color.GREEN);
                File editEntry = (File) entry; 
                Button editButton3 = new Button("edit Name");
                editButton3.setOnAction(e->{
                    String result =  getInputName(false);
                    editEntry.editName(result);
                    nameLabel.setText("\uD83D\uDCC4 "+result);
                     displayEntryList(entry.getParent().getEntries(), entryBox);
                    /* Label dateInfo = getDatetimeStringLabel(editEntry.getCreationDateTime()); */
                });
                Button openFile =  openFileContentButton(editEntry);
                Label dateInfo = getDatetimeStringLabel(editEntry.getCreationDateTime());
                Button deleteButton = deleteButton(entry,entryListContainer);
                entryContainer.getChildren().addAll(nameLabel,editButton3,deleteButton,openFile,dateInfo);
            } 
            entryBox.getChildren().add(entryContainer); 
        }
    }


    public String getTextContent(){
        Dialog<ButtonType> customDialogforText = new Dialog<>();
        customDialogforText.setHeight(100);
        customDialogforText.setWidth(100);
        customDialogforText.setTitle("Edit Text");
        TextField fileTextField = new TextField();
        fileTextField.setPromptText("Edit your file");
        customDialogforText.getDialogPane().setContent(fileTextField);
        customDialogforText.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        Optional<ButtonType> result = customDialogforText.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            String enteredText = fileTextField.getText();
            return enteredText;
        } else {
            return null; 
        }
    };

    public Button openDirectoryButton(Directory directory, VBox entryBox){
        Button customButton = new Button("Open");
        customButton.setOnAction(e -> {
                Stage newStage = new Stage();
                DirectoryScreen newDirectoryScreeen = new DirectoryScreen(directory);
                newDirectoryScreeen.showScreen(newStage);
                displayEntryList(directory.getParent().getEntries(), entryBox);
        });
        return customButton;
    }
    public Button backButton(){
        Button backButton = new Button("Back");
        backButton.setOnAction(e -> {
            Stage newStage = new Stage();
            if(directory.getParent()!=null){
            DirectoryScreen newDirectoryScreeen = new DirectoryScreen(directory.getParent());
            newDirectoryScreeen.showScreen(newStage);
            }       
    });
    return backButton;
    }

    public Label getDatetimeStringLabel(LocalDateTime dateTimeA){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = dateTimeA.format(formatter);
        Label dateInfo = new Label("EditedAt:"+formattedDateTime);
        return dateInfo;
    }

    public Button openBinScreenButton(){
        Button recycleBinButton = new Button("\uD83D\uDDD1️"+"Open RecycleBin");
        recycleBinButton.setOnAction(e->{
            Stage newStage = new Stage();
            RecycleBin recycleBin = Directory.getRecycleBin();
            RecycleBinScreen newRecycleBinScreen = new RecycleBinScreen(recycleBin);
            newRecycleBinScreen.showScreen(newStage);
        });
        return recycleBinButton;
    };

    public Button deleteButton(Entry entry,VBox entryBox){
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e->{
            entry.getParent().deleteEntry(entry);
            displayEntryList(entry.getParent().getEntries(), entryBox);
            return;
        });
        return 
        deleteButton;
    }

    public Button openFileContentButton(File file){
        Button openTextButton = new Button("Open file");
        openTextButton.setOnAction(e->{
            Stage newStage = new Stage();
            FileScreen fileScreen = new FileScreen(file);
            fileScreen.showScreen(newStage);
        });
        return openTextButton;
    }

}
