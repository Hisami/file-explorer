package com.projet1;

import java.time.LocalDateTime;

public class Entry {
    private String name;
    private String path;
    private LocalDateTime creationDateTime;
    private Directory parent;
    // LocalDateTime date;
    // int size;
    public Entry(String name) {
        this.name = name;
        this.creationDateTime = LocalDateTime.now();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String newName) {
        this.name = newName;
    } 

    public String getPath() {
        if(this.parent==null){
            return "/";
        }
        if(this.parent.getPath()=="/"){
            return parent.getPath()+name; 
        }
        return parent.getPath()+"/"+name;
    }

    public void setPath(String newPath) {
        this.path = newPath;
    }

    public void moveItem(Directory newParent) {
        setPath(newParent.getName() + "/" + this.getName());
    }

    public void setParent(Directory directory) {
        this.parent = directory;
    }

    public Directory getParent() {
        return this.parent;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }
    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    // public void setParent(Directory parent) {
    //     this.parent = parent;
    // }
}
