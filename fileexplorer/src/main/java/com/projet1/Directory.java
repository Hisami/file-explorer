package com.projet1;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Directory extends Entry {

    private List<Entry> entries = new ArrayList<Entry>();
    static RecycleBin recycleBin = new RecycleBin();

    public Directory(String name) {
        super(name);
    }

    public Directory(List<Entry> entries, String name) {
        super(name);
        this.entries = entries;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    // public String getName() {
    //     return name;
    // }

   /*  public void attributePath(Entry entry) {
        entry.setPath(this.getName() + "/" + entry.getName());
    } */

    public boolean addEntry(Entry entry) {
        entry.setParent(this);
       // attributePath(entry);
        if (isSameName(entry)) {
            return false;
        }
        return this.entries.add(entry);
    }

    public void editName(String newName) {
        setName(newName);
        setCreationDateTime(LocalDateTime.now());
        for (Entry entry : entries) {
            entry.setParent(this);
            //attributePath(entry);
        }
    }

    public boolean deleteEntry(Entry entry) {
        recycleBin.addItem(entry);
        return this.entries.remove(entry);
    }

    public boolean isSameName(Entry entry) {
        for (Entry e : entries) {
            if (entry.getName().equalsIgnoreCase(e.getName())) {
                return true;
            }
        } return false;
    }

    public void restoreEntry(Entry entry) {
        recycleBin.restoreItem(entry);
        this.addEntry(entry);
    }

    public static RecycleBin getRecycleBin() {
        return recycleBin;
    }

    // public Directory getParent() {
    //     return parent;
    // }

    // public boolean deleteSelf() {
    //     return this.getParent().deleteEntry(this);
    // }
}
