package com.projet1;

import java.util.ArrayList;
import java.util.List;

public class RecycleBin {
    private List<Entry> deletedEntries = new ArrayList<Entry>();

    public RecycleBin() {
    }

    public RecycleBin(List<Entry> deletedEntries) {
        this.deletedEntries = deletedEntries;
    }

    public List<Entry> getDeletedEntries() {
        return deletedEntries;
    }

    public void setDeletedEntries(List<Entry> deletedEntries) {
        this.deletedEntries = deletedEntries;
    }

    public void addItem(Entry deletedEntry) {
        String recycleBinPath = "bin/";

        deletedEntry.setPath(recycleBinPath + deletedEntry.getName());
        this.deletedEntries.add(deletedEntry);
    }

    public void emptyBin() {
        setDeletedEntries(new ArrayList<Entry>());
    }

    public void restoreItem(Entry deletedEntry) {
        this.deletedEntries.remove(deletedEntry);
    }

    
}
