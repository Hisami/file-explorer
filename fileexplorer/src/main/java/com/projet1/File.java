package com.projet1;

import java.time.LocalDateTime;

public class File extends Entry {
    private String text;

    public File(String name) {
        super(name);
    }

    public String editName(String newName) {
        this.setName(newName);
        setCreationDateTime(LocalDateTime.now());
        setPath(this.getPath().replaceAll("\\/[a-zA-Z0-9]+$", "/" + newName));
        return newName;
    }

    public String setText(String newText) {
        setCreationDateTime(LocalDateTime.now());
        return this.text = newText;
    }

    public String getText() {
        return this.text;
    }

}
