import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.projet1.Entry;
import com.projet1.File;
import com.projet1.RecycleBin;

public class TestRecycleBin {
    
    @Test
    void testShouldInstantiate() {
        RecycleBin recycleBin = new RecycleBin();

        assertNotNull(recycleBin);
    }

    @Test
    void testAddItem() {
        RecycleBin recycleBin = new RecycleBin();
        File file1 = new File("file1");

        file1.setPath("root/"+file1.getName());
        recycleBin.addItem(file1);
        List<Entry> result = recycleBin.getDeletedEntries();

        assertNotNull(result);
    }

    @Test
    void emptyBin() {
        File file1 = new File("file1");
        RecycleBin recycleBin = new RecycleBin(new ArrayList<>(List.of(file1)));

        recycleBin.emptyBin();
        List<Entry> result = recycleBin.getDeletedEntries();

        assertTrue(result.isEmpty());
    } 
}
