import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.projet1.Directory;
import com.projet1.File;

public class TestFile {

    @Test
    void testShouldInstantiate() {
        File file = new File("file1");

        assertNotNull(file);
    }

    @Test
    void testShouldAddEntry() {
        Directory root = new Directory("root");
        File file1 = new File("file1");

        boolean result = root.addEntry(file1);

        assertTrue(result);
    }

    @Test
    void testShouldEditName() {
        File file1 = new File("file1");

        file1.setPath("root/file1");
        file1.editName("fileeee1");

        String result = file1.getName();

        assertEquals(result, "fileeee1");
    }

    @Test
    void testMoveItem() {
        File file1 = new File("file1");
        file1.setPath("root/file1");
        Directory user = new Directory("user");

        file1.moveItem(user);
        String result = file1.getPath();

        assertEquals(result, "user/file1");
    }
}
