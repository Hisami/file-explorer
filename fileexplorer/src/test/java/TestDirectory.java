import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.projet1.Directory;
import com.projet1.Entry;
import com.projet1.RecycleBin;

public class TestDirectory {

    @Test
    void testShouldInstantiate() {
        Directory directory = new Directory(null, "root");

        assertNotNull(directory);
    }

    @Test
    void testShowEntryList() {
        Directory user = new Directory(null, "user");
        Directory root = new Directory(List.of(user), "root");

        List<Entry> testArray = new ArrayList<>(List.of(user));
        List<Entry> result = root.getEntries();

        assertEquals(result, testArray);
    }

    @Test
    void testShouldAddEntry() {
        Directory root = new Directory("root");
        Directory user = new Directory("user");

        boolean result = root.addEntry(user);

        assertTrue(result);
    }

    @Test
    void testShouldEditOwnName() {
        Directory root = new Directory("root");

        root.editName("roooooooot");
        String result = root.getName();

        assertEquals(result, "roooooooot");
    }

    @Test
    void testShouldDelete() {
        Directory user = new Directory("user");
        Directory root = new Directory(new ArrayList<>(List.of(user)), "root");

        boolean result = root.deleteEntry(user);

        assertTrue(result);
    }

    @Test
    void testSameName() {
        Directory user = new Directory("user");
        Directory usr = new Directory("user");
        Directory root = new Directory(new ArrayList<>(List.of(user)), "root");

        boolean result = root.isSameName(usr);

        assertTrue(result);
    }

    @Test
    void testAddEntrySameName() {
        Directory user = new Directory("user");
        Directory usr = new Directory("user");
        Directory root = new Directory(new ArrayList<>(List.of(user)), "root");

        boolean result = root.addEntry(usr);

        assertFalse(result);
    }
/* 
    @Test
    void testAttributePath() {
        Directory user = new Directory("user");
        Directory root = new Directory(new ArrayList<>(List.of(user)), "root");

        root.attributePath(user);
        String result = user.getPath();

        assertEquals(result, "root/user");
    } */

    @Test
    void testRestoreEntry() {
        Directory user = new Directory("user");
        Directory root = new Directory("root");
        new RecycleBin(new ArrayList<>(List.of(user)));

        root.restoreEntry(user);
        
        String result = user.getPath();

        assertEquals(result, "/user");
    }

    // @Test
    // void testDeleteSelf() {
    //     Directory user = new Directory("user");
    //     Directory root = new Directory(new ArrayList<>(List.of(user)), "root");
    //     user.setParent(root);
    //     boolean result = user.getParent().deleteEntry(user);

    //     assertTrue(result);;
    // }
}
